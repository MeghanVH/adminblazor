﻿using Furfooz.Admin.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace Furfooz.Admin.Services
{
    public class PointOfInterestService
    {
        private HttpClient http;

        public PointOfInterestService(HttpClient http)
        {
            this.http = http;
        }

        public async Task<IEnumerable<PointOfInterest>> GetAsync()
        {
            return await http.GetFromJsonAsync<IEnumerable<PointOfInterest>>("pointinteret");
        }

        public async Task<IEnumerable<PointOfInterestForm>> GetList()
        {
            return await http.GetFromJsonAsync<IEnumerable<PointOfInterestForm>>("pointinteret");
        }

        public async Task AddAsync(PointOfInterestForm form)
        {
            // fait la même chose que PostAsJsonAsync -> Khun ne fait pas confiance
            string json = JsonConvert.SerializeObject(form);
            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
            await http.PostAsync("pointinteret", content);
        }

        public async Task UpdateAsync(PointOfInterestForm poi)
        {
            await http.PutAsJsonAsync("pointinteret", poi);
        }

    }
}
