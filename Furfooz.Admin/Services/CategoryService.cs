﻿using Furfooz.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Furfooz.Admin.Services
{
    public class CategoryService
    {
        private HttpClient http;

        public CategoryService(HttpClient http)
        {
            this.http = http;
        }

        public async Task<IEnumerable<CategoryModel>> GetAsync()
        {
            return await http.GetFromJsonAsync<IEnumerable<CategoryModel>>("category");
        }

        public async Task AddAsync(CategoryForm form)
        {
            await http.PostAsJsonAsync("category", form);
        }
        public async Task UpdateAsync(CategoryModel cat)
        {
            await http.PutAsJsonAsync("category", cat);
        }
    }
}
