﻿using Furfooz.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Furfooz.Admin.Services
{
    public class FlobetteService
    {
        private HttpClient http;

        public FlobetteService(HttpClient http)
        {
            this.http = http;
        }

        public async Task<IEnumerable<FlobetteForm>> GetProductAsync()
        {
            return await http.GetFromJsonAsync<IEnumerable<FlobetteForm>>("buvette");
        }

        public async Task AddProductAsync(FlobetteForm form)
        {
            await http.PostAsJsonAsync("buvette", form);
        }
        public async Task UpdateProductAsync(FlobetteForm cat)
        {
            await http.PutAsJsonAsync("buvette", cat);
        }

        public async Task<IEnumerable<CategoryProductForm>> GetCatProdAsync()
        {
            return await http.GetFromJsonAsync<IEnumerable<CategoryProductForm>>("categoryproduct");
        }

        public async Task AddCatProdAsync(CategoryProductForm form)
        {
            await http.PostAsJsonAsync("categoryproduct", form);
        }
        public async Task UpdateCatProdAsync(CategoryProductForm cat)
        {
            await http.PutAsJsonAsync("categoryproduct", cat);
        }

        //public async Task DeleteAsync(int id)
        //{
        //    await http.DeleteAsync("buvette/" + id);
        //}
    }
}
