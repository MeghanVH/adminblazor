﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Furfooz.Admin.Models
{
    public class FlobetteForm
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(256)]
        public string Name_fr { get; set; }
        [MaxLength(256)]
        public string Name_en { get; set; }
        [MaxLength(256)]
        public string Name_nl { get; set; }
        public double Price { get; set; }
        public bool IsDeleted { get; set; }
        public int? Category_Id { get; set; }
    }
}
