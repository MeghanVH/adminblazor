﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Furfooz.Admin.Models
{
    public class CategoryProductForm
    {
        public int Id { get; set; }
        [Required]
        public string Name_fr { get; set; }
        [Required]
        public string Name_nl { get; set; }
        [Required]
        public string Name_en { get; set; }
        public bool IsDeleted { get; set; }
    }
}
