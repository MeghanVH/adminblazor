﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Furfooz.Admin.Models
{
    public class PointOfInterestForm
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name_fr { get; set; }
        [MaxLength(100)]
        public string Name_nl { get; set; }
        [MaxLength(100)]
        public string Name_en { get; set; }
        public byte[] Image { get; set; }
        public string mimeType { get; set; }
        public string Description_fr { get; set; }
        public string Description_nl { get; set; }
        public string Description_en { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public double? Interval { get; set; }
        public Boolean IsDeleted { get; set; }
        public int? Category_id { get; set; }
    }
}
