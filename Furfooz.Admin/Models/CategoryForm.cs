﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Furfooz.Admin.Models
{
    public class CategoryForm
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name_fr { get; set; }
        [MaxLength(50)]
        public string Name_nl { get; set; }
        [MaxLength(50)]
        public string Name_en { get; set; }
        public string PinColor { get; set; }
        public bool IsDeleted { get; set; }
    }
}
