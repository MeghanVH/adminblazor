﻿using Furfooz.Admin.Models;
using Furfooz.Admin.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Furfooz.Admin.Pages
{
    public partial class PoIAdd : IDisposable
    {
        public PointOfInterestForm Poi { get; set; }
        public IEnumerable<CategoryModel> CategoryList;
        [Inject]
        public PointOfInterestService Service { get; set; }
        [Inject]
        public CategoryService CatService { get; set; }
        [Inject]
        public IJSRuntime jsRuntime { get; set; }
        [Inject]
        public NavigationManager Nav { get; set; }
        public int selectedId { get; set; }

        protected async override Task OnInitializedAsync()
        {
            Poi = new PointOfInterestForm();
            CategoryList = await CatService.GetAsync();
            tiny();
        }


        public async void tiny()
        {
            await jsRuntime.InvokeVoidAsync("tinymcedestroy");
            await jsRuntime.InvokeVoidAsync("tinymceinit");
        }

        public async Task SubmitAsync()
        {
            try
            {

                Poi.Category_id = selectedId;
                await Service.AddAsync(Poi);
                await jsRuntime.InvokeVoidAsync("success", "OK", "Insertion réussie");
                Nav.NavigateTo("/pointinteret");
            }
            catch (Exception e)
            {
                await jsRuntime.InvokeVoidAsync("error", "Erreur", "Erreur de serveur");
            }
        }
        public void Dispose()
        {
            jsRuntime.InvokeVoidAsync("tinymcedestroy");
        }

        async void OnFileChanged()
        {
            string blob = await jsRuntime.InvokeAsync<string>("GetBlob");
            Poi.Image = Convert.FromBase64String(blob.Split(",")[1]);
            Poi.mimeType = blob.Split(",")[0].Replace("data:", "").Replace(";base64", "");
            StateHasChanged();
        }

        async public void DeletePic()
        {
            Poi.Image = null;
            Poi.mimeType = null;
            await jsRuntime.InvokeVoidAsync("DeleteBlob");
            StateHasChanged();
        }

    }
}
