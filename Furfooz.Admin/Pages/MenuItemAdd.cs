﻿using Furfooz.Admin.Models;
using Furfooz.Admin.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Furfooz.Admin.Pages
{
    public partial class MenuItemAdd
    {
        [Inject]
        public IJSRuntime jsRuntime { get; set; }
        [Inject]
        public FlobetteService Service { get; set; }
        [Inject]
        public NavigationManager Nav { get; set; }
        public FlobetteForm productCreation { get; set; }
        public IEnumerable<CategoryProductForm> catProductList { get; set; }


        protected async override Task OnInitializedAsync()
        {
            productCreation = new FlobetteForm();
            catProductList = await Service.GetCatProdAsync();
        }
        public async Task SubmitProductAsync()
        {
            try
            {
                await Service.AddProductAsync(productCreation);
                await jsRuntime.InvokeVoidAsync("success", "Insertion réussie");
                Nav.NavigateTo("/flobette");
            }
            catch (Exception e)
            {
                await jsRuntime.InvokeVoidAsync("error", "Erreur du serveur");
            }
        }
    }
}
