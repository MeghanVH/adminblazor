﻿using Furfooz.Admin.Models;
using Furfooz.Admin.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Furfooz.Admin.Pages
{

    public partial class PointsOfInterest : IDisposable
    {
        [Inject]
        public PointOfInterestService Service { get; set; }
        [Inject]
        public CategoryService CatServ { get; set;  }
        [Inject]
        public IJSRuntime jsRuntime { get; set; }
        public IEnumerable<PointOfInterestForm> ListPoI { get; set; }
        // Point d'intérêt qu'on update
        public PointOfInterestForm ToUpdate { get { return ListPoI.FirstOrDefault(x => x.Id == selectedPoi); } }
        public IEnumerable<CategoryModel> AllCategories { get; set;  }
        public int selectedId { get; set; }
        public int selectedCategory { get; set; }
        private int _selectedPoi;
        public int selectedPoi { get { return _selectedPoi; } set { _selectedPoi = value; tiny(); } }
        protected async override Task OnInitializedAsync()
        {
            AllCategories = await CatServ.GetAsync();
            ListPoI = await Service.GetList();
        }

        public async Task Update(PointOfInterestForm poi)
        {

            poi.Category_id = selectedCategory;
            try
            {
                await Service.UpdateAsync(poi);
                ListPoI = await Service.GetList();
                if (selectedId != selectedCategory)
                {
                    selectedId = selectedCategory;
                }
                await jsRuntime.InvokeVoidAsync("success", "Demande effectuée", "Modification réussie");
                tiny();
                StateHasChanged();
            }
            catch (Exception)
            {

                await jsRuntime.InvokeVoidAsync("error", "Erreur", "Erreur de serveur");
            }
            
        }

        public async void tiny()
        {
            await jsRuntime.InvokeVoidAsync("tinymcedestroy");
            await jsRuntime.InvokeVoidAsync("tinymceinit");
        }

        public void Dispose()
        {
            jsRuntime.InvokeVoidAsync("tinymcedestroy");
        }

        public void SelectCategory(int id)
        {
            selectedCategory = id;
        }

        public void SoftDelete(int id)
        {
            PointOfInterestForm deletedPoI = ListPoI.FirstOrDefault(x => x.Id == id);
            deletedPoI.IsDeleted = !deletedPoI.IsDeleted;
        }

        async void OnFileChanged()
        {
            string blob = await jsRuntime.InvokeAsync<string>("GetBlob");
            ToUpdate.Image = Convert.FromBase64String(blob.Split(",")[1]);
            ToUpdate.mimeType = blob.Split(",")[0].Replace("data:", "").Replace(";base64", "");
            StateHasChanged();
        }

        async public void DeletePic()
        {
            ToUpdate.Image = null;
            ToUpdate.mimeType = null;
            await jsRuntime.InvokeVoidAsync("DeleteBlob");
            StateHasChanged();
        }

    }
}
