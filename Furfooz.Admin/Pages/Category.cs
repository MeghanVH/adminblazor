﻿using Furfooz.Admin.Models;
using Furfooz.Admin.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Furfooz.Admin.Pages
{
    public partial class Category : IDisposable
    {
        public CategoryForm Cat { get; set; }
        public IEnumerable<CategoryModel> CatList { get; set; }
        public CategoryModel selectedCat { get; set; }
        [Inject]
        public CategoryService Service { get; set; }
        [Inject]
        public IJSRuntime jsRuntime { get; set; }
        public int selectedId { get; set; }
        public CategoryModel ToUpdate { get { return CatList.FirstOrDefault(x => x.Id == selectedId); } }

        protected override async Task OnInitializedAsync()
        {
            selectedCat = new CategoryModel();
            Cat = new CategoryForm();
            CatList = await Service.GetAsync();
        }

        protected override Task OnAfterRenderAsync(bool firstRender)
        {
            jsRuntime.InvokeVoidAsync("select2", "mySelect");
            return base.OnAfterRenderAsync(firstRender);
        }

        public async Task SubmitAsync()
        {
            try
            {
                await Service.AddAsync(Cat);
                await jsRuntime.InvokeVoidAsync("success", "Insertion réussie");
            }
            catch(Exception e)
            {
                await jsRuntime.InvokeVoidAsync("error", "Erreur du serveur");
            }
        }
        public async Task Update(CategoryModel cat)
        {
            try
            {
                await Service.UpdateAsync(cat);
                await jsRuntime.InvokeVoidAsync("success", "Modification effectuée");
            }
            catch (Exception)
            {

                await jsRuntime.InvokeVoidAsync("error", "Erreur du serveur");
            }
        }

        public void GetDescription(int id)
        {
            if (id > 0)
            {
                try
                {
                    selectedCat = CatList.FirstOrDefault(x => x.Id == id);
                    selectedId = selectedCat.Id;
                }
                catch (Exception)
                { 
                    throw;
                }
            }
        }
        public void Dispose()
        {
            jsRuntime.InvokeVoidAsync("select2Destroy");
        }
    }
}
