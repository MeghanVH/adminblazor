﻿using Furfooz.Admin.Models;
using Furfooz.Admin.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Furfooz.Admin.Pages
{
    public partial class Flobette
    {
        [Inject]
        public IJSRuntime jsRuntime { get; set; }
        [Inject]
        public FlobetteService Service { get; set; }
        public CategoryProductForm catProduct { get; set; }
        public CategoryProductForm selectedCat { get; set; }
        public CategoryProductForm catToUpdate { get { return catProductList.FirstOrDefault(x => x.Id == selectedId); } }
        public IEnumerable<CategoryProductForm> catProductList { get; set; }
        public int selectedId { get; set; }
        public IEnumerable<FlobetteForm> productList { get; set; }
        public FlobetteForm productCreation { get; set; }
        public FlobetteForm productToUpdate { get { return productList.FirstOrDefault(x => x.Id == selectedProduct); } }
        public int selectedProduct { get; set; }
        public int selectedCategory { get; set; }
        public int selectedCatId { get; set; }


        protected override async Task OnInitializedAsync()
        {
            catProduct = new CategoryProductForm();
            productCreation = new FlobetteForm();
            catProductList = await Service.GetCatProdAsync();
            productList = await Service.GetProductAsync();

        }
        public async Task SubmitCategoryAsync()
        {
            try
            {
                await Service.AddCatProdAsync(catProduct);
                await jsRuntime.InvokeVoidAsync("success", "Insertion réussie");
            }
            catch (Exception e)
            {
                await jsRuntime.InvokeVoidAsync("error", "Erreur du serveur");
            }
        }
        public void GetDescription(int id)
        {
            if (id > 0)
            {
                try
                {
                    selectedCat = catProductList.FirstOrDefault(x => x.Id == id);
                    selectedId = selectedCat.Id;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        public async Task Update(CategoryProductForm cat)
        {
            try
            {
                await Service.UpdateCatProdAsync(cat);
                await jsRuntime.InvokeVoidAsync("success", "Modification effectuée");
            }
            catch (Exception)
            {

                await jsRuntime.InvokeVoidAsync("error", "Erreur du serveur");
            }
        }

        public void GetProductDescription(int id)
        {
            if (id > 0)
            {
                try
                {
                    selectedCat = catProductList.FirstOrDefault(x => x.Id == id);
                    selectedCatId = selectedCat.Id;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        public async Task UpdateProduct(FlobetteForm product)
        {
            try
            {
                await Service.UpdateProductAsync(product);
                await jsRuntime.InvokeVoidAsync("success", "Modification effectuée");
            }
            catch (Exception)
            {

                await jsRuntime.InvokeVoidAsync("error", "Erreur du serveur");
            }
        }

        public void SoftDelete(int id)
        {
            FlobetteForm deletedProduct = productList.FirstOrDefault(x => x.Id == id);
            deletedProduct.IsDeleted = !deletedProduct.IsDeleted;
        }

        public void SelectCategory(int id)
        {
            selectedCategory = id;
        }

        //public async void Delete(int id)
        //{
        //    id = productToUpdate.Id;
        //    await Service.DeleteAsync(id);
        //}
    }
}
